import requests
from enum import Enum
import os
import json
import time
from urllib.parse import urlparse


class InvalidRequest(Exception):
    pass


class ServerError(Exception):
    pass


class NotFound(Exception):
    pass


class QOS(Enum):
    ONLINE = 'disk+tape'
    OFFLINE = 'tape'
    CACHE_ONLY = 'disk'


class Locality(Enum):
    ONLINE = 'ONLINE'
    NEARLINE = 'NEARLINE'
    ONLINE_AND_NEARLINE = 'ONLINE_AND_NEARLINE'
    UNAVALABLE = 'UNAVAILABLE'
    LOST = 'LOST'


class DCache:
    def check_authentication(self):
        if self.cert and not os.path.exists(self.cert):
            raise FileNotFoundError(f'Missing X509 at path {self.cert}')

    def __init__(self, site_url: str, user=None, password=None, token=None, certificate=None):
        self.site_url = site_url.rstrip('/')
        self.user = user
        self.password = password
        self.token = token
        self.cert = certificate
        self.session = None
        self.check_authentication()

    @staticmethod
    def from_config(config: dict):
        site_url = config['site_url']
        user = config.get('user', None)
        password = config.get('password', None)
        token = config.get('token', None)
        certificate = config.get('cert', None)

        return DCache(site_url, user, password, token, certificate)

    @staticmethod
    def from_config_file(path: str):
        if os.path.exists(path):
            with open(path, 'r') as f_stream:
                return DCache.from_config(json.load(f_stream))
        else:
            raise FileNotFoundError(path)

    def _perform_request(self, path, type, payload=None, params=None):
        full_path = self.site_url + path
        session = self.get_session()

        response = session.request(type, full_path, json=payload, params=params)
        status_code = response.status_code
        if status_code in range(200, 299):
            return response.json()
        elif status_code in [100, 102, 429]:
            return self._perform_request(path, type, payload, **params)
        elif status_code == 404:
            raise NotFound()
        elif status_code in range(400, 499):
            raise InvalidRequest(f'{status_code} {response.reason} - {response.content}')
        elif status_code in range(500, 599):
            raise ServerError(f'{status_code} {response.reason} - {response.content}')
        elif status_code in range(300, 399):
            raise NotImplementedError()

    def _open_session(self):
        session = requests.Session()
        if self.cert:
            session.cert = self.cert
        elif self.token:
            session.headers['Authorization'] = f'Bearer {self.token}'

        return session

    def get_session(self):
        if self.session is None:
            self.session = self._open_session()
        return self.session

    def _close_session(self):
        self.session.close()
        self.session = None

    def active_transfers(self,
                         state=None,
                         door=None,
                         domain=None,
                         prot=None,
                         uid=None,
                         gid=None,
                         vomsgroup=None,
                         path=None,
                         pnfsid=None,
                         client=None):
        url_path = '/api/v1/transfers'
        filter = dict(state=state,
                      door=door,
                      domain=domain,
                      prot=prot,
                      uid=uid,
                      gid=gid,
                      vomsgroup=vomsgroup,
                      path=path,
                      pnfsid=pnfsid,
                      client=client)
        filter = {key: value for key, value in filter.items() if value is not None}

        return self._perform_request(url_path, 'GET', params=filter)

    def doors(self):
        url_path = '/api/v1/doors'
        return self._perform_request(url_path, 'GET')

    def identity(self):
        url_path = '/api/v1/user'
        return self._perform_request(url_path, 'GET')

    def info(self, path: str, recursive=False):
        url_path = '/api/v1/namespace/' + path.rstrip('/').lstrip('/')
        params = dict(locality=True, qos=True)
        if recursive:
            params['children'] = True

        return self._perform_request(url_path, 'GET', params=params)

    def space_tokens(self, id=None):
        url_path = '/api/v1/space/tokens'
        filter = dict(id=id)
        filter = {key: value for key, value in filter.items() if value is not None}

        return self._perform_request(url_path, 'GET', params=filter)

    def mkdir(self, parent_dir: str, dir_name: str):
        dir_name = dir_name.rstrip('/').lstrip('/')
        parent_dir = parent_dir.rstrip('/').lstrip('/')
        url_path = '/api/v1/namespace/' + parent_dir
        return self._perform_request(url_path, 'POST', payload={'action': 'mkdir', 'name': dir_name})

    def mv(self, from_path, to_path):
        to_path = to_path.rstrip('/').lstrip('/')
        from_path = from_path.rstrip('/').lstrip('/')
        url_path = '/api/v1/namespace/' + from_path
        return self._perform_request(url_path, 'POST', payload={'action': 'mv', 'name': to_path})

    def set_qos(self, path, target_qos):
        url_path = '/api/v1/namespace/' + path.rstrip('/').lstrip('/')
        return self._perform_request(url_path, 'POST', payload={'action': 'qos', 'target': target_qos})


class Context:
    def __init__(self, dcache_client: DCache):
        self.dcache_client = dcache_client
        self.username = ''
        self.homeDirectory = ''
        self.rootDirectory = ''

        self._set_context()

    @staticmethod
    def create_from_x509(site_url, certificate_path):
        dcache_client = DCache.from_config(dict(
            site_url=site_url,
            cert=certificate_path))
        return Context(dcache_client)

    def surl_to_path(self, surl: str):
        path = urlparse(surl).path
        if self.rootDirectory == '/':
            return path
        else:
            return path.split(self.rootDirectory)[-1]

    def bring_online(self, surls, *args, async_op=True):
        if isinstance(surls, str):
            surls = [surls]

        for surl in surls:
            path = self.surl_to_path(surl)
            self.dcache_client.set_qos(path, QOS.ONLINE.value)
        return 'OK', 'not_needed'

    def wait_for_online(self, surl, seconds_to_sleep=1):
        while True:
            locality = self.bring_online_pool_single(surl)
            if locality == Locality.ONLINE_AND_NEARLINE.value:
                time.sleep(seconds_to_sleep)
                return

    def abort_bring_online_single(self, surl, *args):
        path = self.surl_to_path(surl)
        self.dcache_client.set_qos(path, QOS.OFFLINE.value)

    def abort_bring_online(self, surls, _):
        if isinstance(surls, str):
            surls = [surls]
        for surl in surls:
            self.abort_bring_online_single(surl)

    def unpin_single(self, surl):
        path = self.surl_to_path(surl)
        self.dcache_client.set_qos(path, QOS.OFFLINE.value)

    def unpin(self, surls):
        if isinstance(surls, str):
            surls = [surls]
        for surl in surls:
            self.unpin_single(surl)

    def bring_online_poll(self, surls, *args):
        if isinstance(surls, str):
            surls = [surls]
        results = []
        for surl in surls:
            results.append(self.bring_online_pool_single(surl))
        return results

    def lstat(self, surl):
        path = self.surl_to_path(surl)
        info = self.dcache_client.info(path)
        stats = dict(
            st_atime='',
            st_ctime='',
            st_dev='',
            st_gid='',
            st_ino='',
            st_mode='',
            st_mtime=info['mtime'],
            st_nlink=info['nlink'],
            st_size=info['size'],
            st_uid='')
        return stats

    def xattrs(self, surl):
        path = self.surl_to_path(surl)
        info = self.dcache_client.info(path)
        xattrs = {
            'user.replicas': '',
            'user.status': info['fileLocality'],
            'srm.type': 'dCache',
            'spacetoken': ''
        }
        return xattrs

    def bring_online_pool_single(self, surl, *args):
        path = self.surl_to_path(surl)
        return self.dcache_client.info(path)['fileLocality']

    def _set_context(self):
        ident = self.dcache_client.identity()
        self.username = ident['username']
        self.rootDirectory = ident['rootDirectory']
