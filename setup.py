import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="dcache_client", # Replace with your own username
    author="Mattia Mancini",
    author_email="mancini@astron.nl",
    description="Client library for the DCACHE api",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.astron.nl/eosc/dcache_client.git",
    packages=setuptools.find_packages(include=['dcacheclient', 'dcacheclient.*']),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    version_config={
        "template": "{tag}",
        "dev_template": "{tag}.dev{ccount}+git.{sha}",
        "dirty_template": "{tag}.dev{ccount}+git.{sha}.dirty",
        "starting_version": "0.0.1",
        "version_callback": None,
        "version_file": 'version',
        "count_commits_from_version_file": True
    },
    setup_requires = ['setuptools-git-versioning'],
)
